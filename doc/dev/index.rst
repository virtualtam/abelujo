Welcome to Abelujo's developer documentation
============================================

Contents:

.. toctree::
   :maxdepth: 2

   abelujo-dev.rst
   choices.rst
   angular-crash-course
   webscraping.rst
   django-dev.rst
   clientside-dev.rst
   deploy-manage.rst
   codingstyle.rst
   editors.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
