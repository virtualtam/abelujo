# Copyright 2014 The Abelujo Developers
# See the COPYRIGHT file at the top-level directory of this distribution

# Abelujo is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Abelujo is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Abelujo.  If not, see <http://www.gnu.org/licenses/>.
django==1.8
django-bootstrap3
pyjade                  # process jade templates.
bootstrap-admin         # bootstrap theme, nice search feature.
pyyaml
psycopg2                # postgresql database engine.
pytz			# timezone

### Django extensions
djangorestframework==3.4 # REST framework. Serve API, serialize models, explore schema, etc
django_extensions==1.7    # more useful commands to manage.py (shell_plus).
mod_wsgi==4.4             # apache server for development.
# mod_wsgi-httpd           # discard the system's apache httpd server if needed.

### utils
toolz                   # pure, lazy, composable functional programming.
tabulate                # pretty print tabular data.
addict                  # dotted notation to dict attributes and more.
unidecode               # ASCII transliteration of unicode text.
distance                # between two strings.
clize==3                # build command line arguments
tqdm                    # progress bar
termcolor               # colorful terminal output
isbnlib<4               # isbn utils
unicodecsv<0.15         # at last unicode support to read and write csv files. Lacking in the standard library.
setuptools>18           # for xhtml2pdf below
xhtml2pdf==0.0.6        # quickfix... see requirements-system.txt
dateparser              # parser for human readable dates

# deployment
whitenoise              # serve static files. Easily, efficiently.
gunicorn==19.4          # wsgi server

huey==1.2               # task queue
redis==2.10
honcho==0.7             # task supervisor based on a Procfile

